package de.sciss.scaladon

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class Report(id          : Id,
                  actionTaken : String
                 )

object Report extends Reader[Report] {

  override final val name = "Report"

  implicit val reads: Reads[Report] = (
    (__ \ "id"          ).read[Id] and
    (__ \ "action_taken").read[String]
  )(Report.apply _)
}
