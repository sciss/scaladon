package de.sciss.scaladon

import akka.actor.ActorSystem
import akka.http.javadsl.model.headers.HttpCredentials
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.HttpMethods.{GET, POST}
import akka.http.scaladsl.model._
import akka.stream.Materializer
import akka.stream.scaladsl.{Flow, Sink, Source}
import de.sciss.scaladon.Implicits.{HttpResponseExtensions, JsValueExtensions, ResponseEntityExtensions}
import de.sciss.scaladon.Mastodon.{DEFAULT_LIMIT, Scope}
import play.api.libs.json._

import java.io.{File, PrintWriter}
import java.nio.file.{Files, Paths}
import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

class Mastodon private(val baseURI: String, appCredentials: AppCredentials, val scopes: Set[Scope])
                      (implicit system: ActorSystem, materializer: Materializer, ec: ExecutionContext) { app =>

  private val flow: Flow[HttpRequest, HttpResponse, Future[Http.OutgoingConnection]] =
    Http().outgoingConnectionHttps(baseURI)

  /** Makes a non-authorized request to the Mastodon instance.
    *
    * @param req  The request to send to the Mastodon instance.
    * @return     A future `HttpResponse`.
    */
  private def makeRequest(req: HttpRequest): Future[HttpResponse] =
    Source.single(req).via(flow).runWith(Sink.head)

  /** Makes an authorized request to the Mastodon instance.
    *
    * @param req    The request to send to the Mastodon instance.
    * @param token  The AccessToken for the authenticated user.
    * @return       A future `HttpResponse`.
    */
  private def authorize(req: HttpRequest, token: AccessToken): Future[HttpResponse] =
    Source.single(req.addCredentials(token.credentials)).via(flow).runWith(Sink.head)

  /** Logs the user into the Mastodon instance and returns a future access token.
    *
    * @param username The username to log in with (for Mastodon, this is the account's e-mail address)
    * @param password The password of the user.
    * @param scopes   The scopes to use when logging in. Must be a set of the application's scopes.
    * @return         A future access token.
    */
  def login(username: String,
            password: String,
            scopes  : Set[Scope] = app.scopes
           ): Future[AccessToken] = {
    val entity = Json.obj(
      "client_id"     -> appCredentials.clientId,
      "client_secret" -> appCredentials.clientSecret,
      "grant_type"    -> "password",
      "username"      -> username,
      "password"      -> password,
      "scope"         -> scopes.mkString(" ")
    ) .toJsonEntity
    val req = HttpRequest(POST, uri = "/oauth/token", entity = entity)

    makeRequest(req).flatMap(xhr => xhr.entity.toJsValue.map { json =>
      val token = (json \ "access_token").as[String]
      AccessToken(HttpCredentials.createOAuth2BearerToken(token))
    })
  }

  /** Toots a text status.
    *
    * To include media attachments, use `Statuses.uploadMedia` first, and flat-map the
    * resulting attachment's `id` to the `mediaIds` argument of `Statuses.post`.
    *
    * If the toot is sensitive, use `Statuses.post` instead and include `sensitive = true`.
    *
    * @param status       The status to toot.
    * @param visibility   The visibility of the toot.
    * @param inReplyToId  An optional id of the status this status should be in reply to.
    * @param spoilerText  The spoiler text for a status with a content warning.
    * @param token        The AccessToken for the authenticated user.
    * @return             A future response that may contain the new status or an error.
    */
  def toot(status     : String,
           visibility : Visibility      = Visibility.Default,
           inReplyToId: Option[Id]      = None,
           spoilerText: Option[String]  = None
          )(implicit token: AccessToken): Future[Status] =
    Statuses.post(
      status      = status,
      visibility  = visibility,
      inReplyToId = inReplyToId,
      spoilerText = spoilerText,
    )

  /** An object containing the methods described in the "Accounts" section of the Mastodon API documentation.
    * (https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#accounts)
    */
  object Accounts {
    /** Fetches an account.
      *
      * @param id     The id of the account to fetch.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response that may contain the desired account or an error.
      */
    def fetch(id: Id)(implicit token: AccessToken): Future[Account] = {
      val req = HttpRequest(GET, uri = s"/api/v1/accounts/$id")

      authorize(req, token).flatMap(_.as(Account))
    }

    /** Fetches the account of the authenticated user.
      *
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response that may contain the authenticated user's account or an error.
      */
    def fetchAuthenticated()(implicit token: AccessToken): Future[Account] = {
      val req = HttpRequest(GET, uri = "/api/v1/accounts/verify_credentials")

      authorize(req, token).flatMap(_.as(Account))
    }

    //TODO: def updateInformation()

    /** Fetches the accounts following the given account.
      *
      * @param id       The account id to fetch followers accounts for.
      * @param limit    Maximum number of accounts to fetch.
      * @param maxId    Limits the accounts to those with an id less than or equal to this value.
      * @param sinceId  Limits the accounts to those with an id greater than this value.
      * @param token    The AccessToken for the authenticated user.
      * @return         A future response that may contain the accounts or an error.
      */
    def fetchFollowers(id     : Id,
                       limit  : Int         = DEFAULT_LIMIT,
                       maxId  : Option[Id]  = None,
                       sinceId: Option[Id]  = None,
                      )(implicit token: AccessToken): Future[Seq[Account]] = {
      val entity = Json.obj(
        "limit"     -> limit,
        "max_id"    -> maxId,
        "since_id"  -> sinceId,
      ).toJsonEntity
      val req = HttpRequest(GET, uri = s"/api/v1/accounts/$id/followers", entity = entity)

      authorize(req, token).flatMap(_.asSeq(Account))
    }

    /** Fetches the accounts the given account is following.
      * @param id       The account id to fetch following accounts for.
      * @param limit    Maximum number of accounts to fetch.
      * @param maxId    Limits the accounts to those with an id less than or equal to this value.
      * @param sinceId  Limits the accounts to those with an id greater than this value.
      * @param token    The AccessToken for the authenticated user.
      * @return         A future response that may contain the accounts or an error.
      */
    def fetchFollowing(id     : Id,
                       limit  : Int         = DEFAULT_LIMIT,
                       maxId  : Option[Id]  = None,
                       sinceId: Option[Id]  = None,
                      )(implicit token: AccessToken): Future[Seq[Account]] = {
      val entity = Json.obj(
        "limit"     -> limit,
        "max_id"    -> maxId,
        "since_id"  -> sinceId
      ).toJsonEntity
      val req = HttpRequest(GET, uri = s"/api/v1/accounts/$id/following", entity = entity)

      authorize(req, token).flatMap(_.asSeq(Account))
    }

    /** Fetches statuses post by the given account.
      * @param id             The account id to fetch statuses for.
      * @param limit          Maximum number of statuses to fetch.
      * @param maxId          Limits the statuses to those with an id less than or equal to this value.
      * @param sinceId        Limits the statuses to those with an id greater than this value.
      * @param onlyMedia      Whether or not to fetch only media posts.
      * @param excludeReplies Whether or not to exclude replies.
      * @param token          The AccessToken for the authenticated user.
      * @return               A future response that may contain the statuses or an error.
      */
    def fetchStatuses(id            : Id,
                      limit         : Int         = DEFAULT_LIMIT,
                      maxId         : Option[Id]  = None,
                      sinceId       : Option[Id]  = None,
                      onlyMedia     : Boolean     = false,
                      excludeReplies: Boolean     = false,
                     )(implicit token: AccessToken): Future[Seq[Status]] = {
      val entity = Json.obj(
        "limit"           -> limit,
        "max_id"          -> maxId,
        "since_id"        -> sinceId,
        "only_media"      -> onlyMedia,
        "exclude_replies" -> excludeReplies,
      ).toJsonEntity
      val req = HttpRequest(GET, uri = s"/api/v1/accounts/$id/statuses", entity = entity)

      authorize(req, token).flatMap(_.asSeq(Status))
    }

    /** Follows the given account.
      *
      * @param id     The account id to follow.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response that may contain the new relationship or an error.
      */
    def follow(id: Id)(implicit token: AccessToken): Future[Relationship] = {
      val req = HttpRequest(POST, uri = s"/api/v1/accounts/$id/follow")

      authorize(req, token).flatMap(_.as(Relationship))
    }

    /** Unfollows the given account.
      * @param id     The account id to unfollow.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response that may contain the new relationship or an error.
      */
    def unfollow(id: Id)(implicit token: AccessToken): Future[Relationship] = {
      val req = HttpRequest(POST, uri = s"/api/v1/accounts/$id/unfollow")

      authorize(req, token).flatMap(_.as(Relationship))
    }

    /** Blocks the given account.
      * @param id     The account id to block.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response that may contain the new relationship or an error.
      */
    def block(id: Id)(implicit token: AccessToken): Future[Relationship] = {
      val req = HttpRequest(POST, uri = s"/api/v1/accounts/$id/block")

      authorize(req, token).flatMap(_.as(Relationship))
    }

    /** Unblocks the given account.
      * @param id     The account id to unblock.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response that may contain the new relationship or an error.
      */
    def unblock(id: Id)(implicit token: AccessToken): Future[Relationship] = {
      val req = HttpRequest(POST, uri = s"/api/v1/accounts/$id/unblock")

      authorize(req, token).flatMap(_.as(Relationship))
    }

    /** Mutes the given account.
      * @param id     The account id to mute.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response that may contain the new relationship or an error.
      */
    def mute(id: Id)(implicit token: AccessToken): Future[Relationship] = {
      val req = HttpRequest(POST, uri = s"/api/v1/accounts/$id/mute")

      authorize(req, token).flatMap(_.as(Relationship))
    }

    /** Unmutes the given account.
      *
      * @param id     The account id to unmute.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response that may contain the new relationship or an error.
      */
    def unmute(id: Id)(implicit token: AccessToken): Future[Relationship] = {
      val req = HttpRequest(POST, uri = s"/api/v1/accounts/$id/unmute")

      authorize(req, token).flatMap(_.as(Relationship))
    }

    /** Fetches relationships for a list of accounts.
      *
      * @param ids    The accounts to fetch relationships for.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response that may contain the relationships or an error.
      */
    def fetchRelationships(ids: Seq[Id] = Seq.empty)
                          (implicit token: AccessToken): Future[Seq[Relationship]] = {
      val entity = Json.obj(
        "id" -> ids
      ).toJsonEntity
      val req = HttpRequest(GET, uri = s"/api/v1/accounts/relationships", entity = entity)

      authorize(req, token).flatMap(_.asSeq(Relationship))
    }

    /** Searches accounts.
      *
      * @param query  The query to search for.
      * @param limit  The limit of accounts to return.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response that may contain the resulting accounts or an error.
      */
    def search(query: String,
               limit: Int = DEFAULT_LIMIT
              )(implicit token: AccessToken): Future[Seq[Account]] = {
      val entity = Json.obj(
        "q" -> query,
        "limit" -> limit
      ).toJsonEntity
      val req = HttpRequest(GET, uri = "/api/v1/accounts/search", entity = entity)

      authorize(req, token).flatMap(_.asSeq(Account))
    }
  }

  /** An object containing the methods described in the "Blocks" section of the Mastodon API documentation.
    * (https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#blocks)
    */
  object Blocks {
    /** Fetches accounts the authenticated user is blocking.
      *
      * @param limit    Maximum number of accounts to fetch.
      * @param maxId    Limits the accounts to those with an id less than or equal to this value.
      * @param sinceId  Limits the accounts to those with an id greater than this value.
      * @param token    The AccessToken for the authenticated user.
      * @return         A future response that may contain the blocked accounts or an error.
      */
    def fetch(limit   : Int         = DEFAULT_LIMIT,
              maxId   : Option[Id]  = None,
              sinceId : Option[Id]  = None,
             )(implicit token: AccessToken): Future[Seq[Account]] = {
      val entity = Json.obj(
        "limit"     -> limit,
        "max_id"    -> maxId,
        "since_id"  -> sinceId,
      ).toJsonEntity
      val req = HttpRequest(GET, uri = "/api/v1/blocks", entity = entity)

      authorize(req, token).flatMap(_.asSeq(Account))
    }
  }

  /** An object containing the methods described in the "Favourites" section of the Mastodon API documentation.
    * (https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#favourites)
    */
  object Favourites {
    /** Fetches statuses the authenticated user has favourited.
      *
      * @param limit    Maximum number of statuses to fetch.
      * @param maxId    Limits the statuses to those with an id less than or equal to this value.
      * @param sinceId  Limits the statuses to those with an id greater than this value.
      * @param token    The AccessToken for the authenticated user.
      * @return         A future response that may contain the favourited statuses or an error.
      */
    def fetch(limit   : Int         = DEFAULT_LIMIT,
              maxId   : Option[Id]  = None,
              sinceId : Option[Id]  = None,
             )(implicit token: AccessToken): Future[Seq[Status]] = {
      val entity = Json.obj(
        "limit"     -> limit,
        "max_id"    -> maxId,
        "since_id"  -> sinceId,
      ).toJsonEntity
      val req = HttpRequest(GET, uri = "/api/v1/favourites", entity = entity)

      authorize(req, token).flatMap(_.asSeq(Status))
    }
  }

  /** An object containing the methods described in the "Follows" section of the Mastodon API documentation.
    * (https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#follows)
    */
  object Follows {
    /** Follows a user by their user URI.
      *
      * @param userUri  The user URI of the account to follow.
      * @param token    The AccessToken for the authenticated user.
      * @return         A future response containing the followed account or an error.
      */
    def follow(userUri: String)(implicit token: AccessToken): Future[Account] = {
      val entity = Json.obj(
        "uri" -> userUri
      ).toJsonEntity
      val req = HttpRequest(POST, uri = "/api/v1/follows", entity = entity)

      authorize(req, token).flatMap(_.as(Account))
    }
  }

  /** An object containing the methods described in the "Follow Requests" section of the Mastodon API documentation.
    * (https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#follow-requests)
    */
  object FollowRequests {
    /** Fetches follow requests
      *
      * @param limit    Maximum number of accounts to fetch.
      * @param maxId    Limits the accounts to those with an id less than or equal to this value.
      * @param sinceId  Limits the accounts to those with an id greater than this value.
      * @param token    The AccessToken for the authenticated user.
      * @return         A future response containing the accounts requesting to follow the user or an error.
      */
    def fetchFollows(limit  : Int         = DEFAULT_LIMIT,
                     maxId  : Option[Id]  = None,
                     sinceId: Option[Id]  = None,
                    )(implicit token: AccessToken): Future[Seq[Account]] = {
      val entity = Json.obj(
        "limit"     -> limit,
        "max_id"    -> maxId,
        "since_id"  -> sinceId,
      ).toJsonEntity
      val req = HttpRequest(GET, uri = "/api/v1/follow_requests", entity = entity)

      authorize(req, token).flatMap(_.asSeq(Account))
    }

    /** Authorizes a follow request.
      *
      * @param id     The id of the account to authorize.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response containing a Unit (empty object) or an error.
      */
    def authorizeFollow(id: Id)(implicit token: AccessToken): Future[Unit] = {
      val entity = Json.obj(
        "id" -> id
      ).toJsonEntity
      val req = HttpRequest(POST, uri = "/api/v1/follow_requests/authorize", entity = entity)

      authorize(req, token).flatMap(_.as(Empty))
    }

    /** Rejects a follow request.
      *
      * @param id     The id of the account to reject.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response containing a Unit (empty object) or an error.
      */
    def rejectFollow(id: Id)(implicit token: AccessToken): Future[Unit] = {
      val entity = Json.obj(
        "id" -> id
      ).toJsonEntity
      val req = HttpRequest(POST, uri = "/api/v1/follow_requests/reject", entity = entity)

      authorize(req, token).flatMap(_.as(Empty))
    }
  }

  /** An object containing the methods described in the "Instances" section of the Mastodon API documentation.
    * (https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#instances)
    */
  object Instances {
    /** Fetches information about the connected instance.
      *
      * @return A future response containing the instance information or an error.
      */
    def fetchInformation: Future[Instance] = {
      val req = HttpRequest(GET, uri = "/api/v1/instance")

      makeRequest(req).flatMap(_.as(Instance))
    }
  }

  /** An object containing the methods described in the "Mutes" section of the Mastodon API documentation.
    * (https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#mutes)
    */
  object Mutes {
    /** Fetches accounts the authenticated user is muting.
      *
      * @param limit    Maximum number of accounts to fetch.
      * @param maxId    Limits the accounts to those with an id less than or equal to this value.
      * @param sinceId  Limits the accounts to those with an id greater than this value.
      * @param token    The AccessToken for the authenticated user.
      * @return         A future response containing the muted accounts or an error.
      */
    def fetch(limit   : Int         = DEFAULT_LIMIT,
              maxId   : Option[Id]  = None,
              sinceId : Option[Id]  = None,
             )(implicit token: AccessToken): Future[Seq[Account]] = {
      val entity = Json.obj(
        "limit"     -> limit,
        "max_id"    -> maxId,
        "since_id"  -> sinceId,
      ).toJsonEntity
      val req = HttpRequest(GET, uri = "/api/v1/mutes", entity = entity)

      authorize(req, token).flatMap(_.asSeq(Account))
    }
  }

  /** An object containing the methods described in the "Notifications" section of the Mastodon API documentation.
    * (https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#notifications)
    */
  object Notifications {
    /** Clears notifications.
      *
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response containing a Unit or an error.
      */
    def clear()(implicit token: AccessToken): Future[Unit] = {
      val req = HttpRequest(POST, uri = s"/api/v1/notifications/clear")

      authorize(req, token).flatMap(_.as(Empty))
    }

    /** Fetches notifications.
      *
      * @param token    The AccessToken for the authenticated user.
      * @param limit    Maximum number of notifications to fetch.
      * @param maxId    Limits the notifications to those with an id less than or equal to this value.
      * @param sinceId  Limits the notifications to those with an id greater than this value.
      * @return         A future response containing the notifications or an error.
      */
    def fetch(limit   : Int         = DEFAULT_LIMIT,
              maxId   : Option[Id]  = None,
              sinceId : Option[Id]  = None,
             )(implicit token: AccessToken): Future[Seq[Notification]] = {
      val entity = Json.obj(
        "limit"     -> limit,
        "max_id"    -> maxId,
        "since_id"  -> sinceId,
      ).toJsonEntity
      val req = HttpRequest(GET, uri = "/api/v1/notifications", entity = entity)

      authorize(req, token).flatMap(_.asSeq(Notification))
    }

    /** Fetches a notification.
      *
      * @param id     The id of the notification to fetch.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response containing the notification or an error.
      */
    def fetch(id: Id)(implicit token: AccessToken): Future[Notification] = {
      val req = HttpRequest(GET, uri = s"/api/v1/notifications/$id")

      authorize(req, token).flatMap(_.as(Notification))
    }
  }

  /** An object containing the methods described in the "Reports" section of the Mastodon API documentation.
    * (https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#reports)
    */
  object Reports {
    /** Fetches reports made by the authenticated user.
      *
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response containing the reports or an error.
      */
    def fetch()(implicit token: AccessToken): Future[Seq[Report]] = {
      val req = HttpRequest(GET, uri = "/api/v1/reports")

      authorize(req, token).flatMap(_.asSeq(Report))
    }

    /** Reports the status of a user.
      *
      * @param accountId  The account id of the user to report.
      * @param statusIds  The statuses to report.
      * @param comment    A comment about the report
      * @param token      The AccessToken for the authenticated user.
      * @return           A future response containing the report or an error.
      */
    def report(accountId: Id, statusIds: Seq[Id], comment: String)
              (implicit token: AccessToken): Future[Report] = {
      val entity = Json.obj(
        "account_id"  -> accountId,
        "status_ids"  -> statusIds,
        "comment"     -> comment
      ).toJsonEntity
      val req = HttpRequest(POST, uri = "/api/v1/reports", entity = entity)

      authorize(req, token).flatMap(_.as(Report))
    }
  }

  /** An object containing the methods described in the "Search" section of the Mastodon API documentation.
    * (https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#search)
    */
  object Search {
    /** Searches content.
      *
      * @param query            The search query.
      * @param resolveNonLocal  Whether or not to resolve results from non-local accounts.
      * @return                 A future response containing the search results or an error.
      */
    def content(query: String,
                resolveNonLocal: Boolean = false): Future[Results] = {
      val entity = Json.obj(
        "q"       -> query,
        "resolve" -> resolveNonLocal
      ).toJsonEntity
      val req = HttpRequest(GET, uri = "/api/v1/search", entity = entity)

      makeRequest(req).flatMap(_.as(Results))
    }
  }

  /** An object containing the methods described in the "Statuses" section of the Mastodon API documentation.
    * (https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#statuses)
    */
  object Statuses {
    /** Fetches a status.
      *
      * @param id   The id of the status to fetch.
      * @return     A future response containing the status or an error.
      */
    def fetch(id: Id): Future[Status] = {
      val req = HttpRequest(GET, uri = s"/api/v1/statuses/$id")

      makeRequest(req).flatMap(_.as(Status))
    }

    /** Fetches a status context.
      *
      * @param id   The id of the status to fetch the context for.
      * @return     A future response containing the status context or an error.
      */
    def fetchContext(id: Id): Future[Context] = {
      val req = HttpRequest(GET, uri = s"/api/v1/statuses/$id/context")

      makeRequest(req).flatMap(_.as(Context))
    }

    /** Fetches a status card.
      *
      * @param id   The id of the status to fetch the card for.
      * @return     A future response containing the status card or an error.
      */
    def fetchCard(id: Id): Future[Card] = {
      val req = HttpRequest(GET, uri = s"/api/v1/statuses/$id/card")

      makeRequest(req).flatMap(_.as(Card))
    }

    // XXX TODO : is this 'favourites' now?
    /** Fetches who favourited a status.
      *
      * @param id       The id of the status to fetch the favourited accounts for.
      * @param limit    Maximum number of accounts to fetch.
      * @param maxId    Limits the accounts to those with an id less than or equal to this value.
      * @param sinceId  Limits the accounts to those with an id greater than this value.
      * @return         A future response containing the accounts or an error.
      */
    def favouritedBy(id     : Id,
                     limit  : Int         = DEFAULT_LIMIT,
                     maxId  : Option[Id]  = None,
                     sinceId: Option[Id]  = None,
                    ): Future[Seq[Account]] = {
      val entity = Json.obj(
        "limit"     -> limit,
        "max_id"    -> maxId,
        "since_id"  -> sinceId,
      ).toJsonEntity
      val req = HttpRequest(GET, uri = s"/api/v1/statuses/$id/favourited_by", entity = entity)

      makeRequest(req).flatMap(_.asSeq(Account))
    }

    /** Fetches who reblogged a status.
      *
      * @param id       The id of the status to fetch the reblogged accounts for.
      * @param limit    Maximum number of accounts to fetch.
      * @param maxId    Limits the accounts to those with an id less than or equal to this value.
      * @param sinceId  Limits the accounts to those with an id greater than this value.
      * @return         A future response containing the accounts or an error.
      */
    def rebloggedBy(id      : Id,
                    limit   : Int         = DEFAULT_LIMIT,
                    maxId   : Option[Id]  = None,
                    sinceId : Option[Id]  = None,
                   ): Future[Seq[Account]] = {
      val entity = Json.obj(
        "limit"     -> limit,
        "max_id"    -> maxId,
        "since_id"  -> sinceId,
      ).toJsonEntity
      val req = HttpRequest(GET, uri = s"/api/v1/statuses/$id/reblogged_by", entity = entity)

      makeRequest(req).flatMap(_.asSeq(Account))
    }

    def post(status     : String,
             visibility : Visibility      = Visibility.Default,
             sensitive  : Boolean         = false,
             inReplyToId: Option[Id]      = None,
             spoilerText: Option[String]  = None,
             mediaIds   : Seq[Id]         = Nil,
            )(implicit token: AccessToken): Future[Status] = {
      val entity = Json.obj(
        "status"          -> status,
        "media_ids"       -> mediaIds,
        "sensitive"       -> sensitive,
        "in_reply_to_id"  -> inReplyToId,
        "spoiler_text"    -> spoilerText,
        "visibility"      -> visibility.toString
      ).toJsonEntity
      val req = HttpRequest(POST, uri = s"/api/v1/statuses", entity = entity)

      authorize(req, token).flatMap(_.as(Status))
    }

    /** Deletes a status.
      *
      * @param id     The id of the status to delete.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response containing a Unit (empty object) or an error.
      */
    def delete(id: Id)(implicit token: AccessToken): Future[Unit] = {
      val req = HttpRequest(HttpMethods.DELETE, uri = s"/api/v1/statuses/$id")

      authorize(req, token).flatMap(_.as(Empty))
    }

    /** Reblogs a status.
      *
      * @param id     The id of the status to reblog.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response containing the reblogged status or an error.
      */
    def reblog(id: Id)(implicit token: AccessToken): Future[Status] = {
      val req = HttpRequest(POST, uri = s"/api/v1/statuses/$id/reblog")

      authorize(req, token).flatMap(_.as(Status))
    }

    /** Unreblogs a status.
      *
      * @param id     The id of the status to unreblog.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response containing the unreblogged status or an error.
      */
    def unreblog(id: Id)(implicit token: AccessToken): Future[Status] = {
      val req = HttpRequest(POST, uri = s"/api/v1/statuses/$id/unreblog")

      authorize(req, token).flatMap(_.as(Status))
    }

    /** Favourites a status.
      *
      * @param id     The id of the status to favourite.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response containing the favourited status or an error.
      */
    def favourite(id: Id)(implicit token: AccessToken): Future[Status] = {
      val req = HttpRequest(POST, uri = s"/api/v1/status/$id/favourite")

      authorize(req, token).flatMap(_.as(Status))
    }

    /** Unfavourites a status.
      *
      * @param id     The id of the status to unfavourite.
      * @param token  The AccessToken for the authenticated user.
      * @return       A future response containing the unfavourited status or an error.
      */
    def unfavourite(id: Id)(implicit token: AccessToken): Future[Status] = {
      val req = HttpRequest(POST, uri = s"/api/v1/status/$id/unfavourite")

      authorize(req, token).flatMap(_.as(Status))
    }
    /** Creates an attachment by uploading media.
      *
      * @param f            The file to upload, such as image or video.
      * @param description  optional text description for accessibility. It is highly recommended
      *                     to include this.
      * @param thumbnail    optional thumbnail file. If present, requires API v2.
      * @param focus        optional cropping focus, with x and y between -1 and +1.
      * @param token        The AccessToken for the authenticated user.
      * @return             A future response containing the resulting attachment or an error.
      */
    def uploadMedia(f           : File,
                    description : Option[String],
                    thumbnail   : Option[File]        = None,
                    focus       : Option[MediaFocus]  = None
                   )(implicit token: AccessToken): Future[Attachment] = {

      val filePart = Multipart.FormData.BodyPart.fromPath(
        name        = "file",
        contentType = MediaTypes.`application/octet-stream`,
        file        = f.toPath,
        chunkSize   = 100000
      )

      val descriptionPart = description.map(Multipart.FormData.BodyPart.Strict("description", _))

      val thumbnailPart = thumbnail.map { tn =>
        Multipart.FormData.BodyPart.fromPath(
          name        = "thumbnail",
          contentType = MediaTypes.`application/octet-stream`,
          file        = tn.toPath,
          chunkSize   = 100000
        )
      }

      val focusPart = focus.map { case MediaFocus(x, y) =>
        Multipart.FormData.BodyPart.Strict("focus",
          String.format(Locale.US, "%1.4f,%1.4f", x.asInstanceOf[AnyRef], y.asInstanceOf[AnyRef]))
      }

      val formData = Multipart.FormData(
        Source(
          filePart :: List(descriptionPart, thumbnailPart, focusPart).flatten
        )
      )
      val futEntity = Marshal(formData).to[RequestEntity]
      futEntity.flatMap { e =>
        val apiVersion = if (thumbnail.isEmpty) 1 else 2
        val req = HttpRequest(POST, uri = s"/api/v$apiVersion/media", entity = e)
        authorize(req, token).flatMap(_.as(Attachment))
      }
    }
  }

  /** An object containing the methods described in the "Timelines" section of the Mastodon API documentation.
    * (https://github.com/mastodon/documentation/blob/master/content/en/methods/timelines.md)
    */
  object Timelines {
    /** Fetches the user's home timeline.
      *
      * @param localOnly  Whether or not to limit timeline to local results only.
      * @param limit      Maximum number of statuses to fetch.
      * @param maxId      Limits the statuses to those with an id less than or equal to this value.
      * @param sinceId    Limits the statuses to those with an id greater than this value.
      * @param token      The AccessToken for the authenticated user.
      * @return           A future response containing the statuses or an error.
      */
    def fetchHome(localOnly : Boolean     = false,
                  limit     : Int         = DEFAULT_LIMIT,
                  maxId     : Option[Id]  = None,
                  sinceId   : Option[Id]  = None,
                 )(implicit token: AccessToken): Future[Seq[Status]] = {
      val entity = Json.obj(
        "local"     -> localOnly,
        "limit"     -> limit,
        "max_id"    -> maxId,
        "since_id"  -> sinceId,
      ).toJsonEntity
      val req = HttpRequest(GET, uri = s"/api/v1/timelines/home", entity = entity)

      authorize(req, token).flatMap(_.asSeq(Status))
    }

    /** Fetches the public timeline.
      *
      * @param localOnly  Whether or not to limit timeline to local results only.
      * @param limit      Maximum number of statuses to fetch.
      * @param maxId      Limits the statuses to those with an id less than or equal to this value.
      * @param sinceId    Limits the statuses to those with an id greater than this value.
      * @param minId      Limits the statuses to those with an id greater than this value.
      * @return           A future response containing the statuses or an error.
      */
    def fetchPublic(localOnly : Boolean     = false,
                    limit     : Int         = DEFAULT_LIMIT,
                    maxId     : Option[Id]  = None,
                    sinceId   : Option[Id]  = None,
                    minId     : Option[Id]  = None,
                   ): Future[Seq[Status]] = {
      val entity = Json.obj(
        "local"     -> localOnly,
        "limit"     -> limit,
        "max_id"    -> maxId,
        "since_id"  -> sinceId,
        "min_id"    -> minId,
      ).toJsonEntity
      val req = HttpRequest(GET, uri = s"/api/v1/timelines/public", entity = entity)

      makeRequest(req).flatMap(_.asSeq(Status))
    }

    /** Fetches a timeline for a hashtag.
      *
      * @param hashtag    The hashtag to get the timeline for.
      * @param localOnly  Whether or not to limit timeline to local results only.
      * @param limit      Maximum number of statuses to fetch.
      * @param maxId      Limits the statuses to those with an id less than or equal to this value.
      * @param sinceId    Limits the statuses to those with an id greater than this value.
      * @param minId      Limits the statuses to those with an id greater than this value.
      * @return           A future response containing the statuses or an error.
      */
    def fetchForHashtag(hashtag   : String,
                        localOnly : Boolean     = false,
                        limit     : Int         = DEFAULT_LIMIT,
                        maxId     : Option[Id]  = None,
                        sinceId   : Option[Id]  = None,
                        minId     : Option[Id]  = None,
                       ): Future[Seq[Status]] = {
      val entity = Json.obj(
        "local"     -> localOnly,
        "limit"     -> limit,
        "max_id"    -> maxId,
        "since_id"  -> sinceId,
        "min_id"    -> minId,
      ).toJsonEntity
      val req = HttpRequest(GET, uri = s"/api/v1/timelines/tag/$hashtag", entity = entity)

      makeRequest(req).flatMap(_.asSeq(Status))
    }
  }

  // cf. https://docs.joinmastodon.org/methods/timelines/conversations/

  /** Direct conversations with other participants.
    *
    * Currently, just threads containing a post with "direct" visibility.
    */
  object Conversations {
    /** Fetches the status of conversations.
      *
      * @param limit      Maximum number of statuses to fetch. The default is 20, the maximum is 40.
      * @param maxId      Limits the statuses to those with an id less than or equal to this value.
      * @param sinceId    Limits the statuses to those with an id greater than this value.
      * @param minId      Limits the statuses to those with an id greater than this value.
      * @return           A future response containing the statuses or an error.
      */
    def fetch(limit     : Int         = 20,
              maxId     : Option[Id]  = None,
              sinceId   : Option[Id]  = None,
              minId     : Option[Id]  = None,
             )(implicit token: AccessToken): Future[Seq[Conversation]] = {
      val entity = Json.obj(
        "limit"     -> limit,
        "max_id"    -> maxId,
        "since_id"  -> sinceId,
        "min_id"    -> minId,
      ).toJsonEntity
      val req = HttpRequest(GET, uri = s"/api/v1/conversations", entity = entity)

      authorize(req, token).flatMap(_.asSeq(Conversation))
    }
  }
}

object Mastodon {
  final val DEFAULT_STORAGE_LOC : String = System.getProperty("user.home") + "/.scaladon"
  final val DEFAULT_REDIRECT_URI: String = "urn:ietf:wg:oauth:2.0:oob"

  final val DEFAULT_LIMIT = 40

  private def appDataPath(baseURI: String, clientName: String, storageLoc: String): String =
    s"$storageLoc/$baseURI/apps/$clientName.mdon"

  private def loadAppData(baseURI: String, clientName: String, storageLoc: String): Option[AppCredentials] = {
    val path = appDataPath(baseURI = baseURI, clientName = clientName, storageLoc = storageLoc)
    if (Files.exists(Paths.get(path))) {
      val src = io.Source.fromFile(path)
      val txt = try { src.getLines().mkString } finally { src.close() }
      Some(Json.parse(txt).as[AppCredentials])
    } else None
  }

  private def saveAppData(baseURI: String, clientName: String, credentials: AppCredentials,
                          storageLoc: String): Unit = {
    val path    = appDataPath(baseURI = baseURI, clientName = clientName, storageLoc = storageLoc)
    val data    = Json.toJson(credentials).toString
    val writer  = new PrintWriter(new File(path))
    try { writer.write(data) } finally writer.close()
  }

  def deleteAppData(baseURI: String, clientName: String, storageLoc: String): Boolean = {
    val path = appDataPath(baseURI = baseURI, clientName = clientName, storageLoc = storageLoc)
    new File(path).delete()
  }

  sealed trait Scope { override def toString: String = name; def name: String }
  object Scope {
    case object Read    extends Scope { def name = "read"   }
    case object Write   extends Scope { def name = "write"  }
    case object Follow  extends Scope { def name = "follow" }

    def all: Set[Scope] = Set(Read, Write, Follow)
  }

  /** Registers a Mastodon based application.
    *
    * @param baseURI        the host, e.g. `"botsin.space"`
    * @param clientName     the name of your application
    * @param scopes         the types of access granted to the application
    * @param redirectURIs   the URL used for redirecting OAuth processing
    * @param storageLoc     the base path on your local system where configuration files will be saved.
    *                       the actual configuration file will be
    *                       `"<storageLoc>/<baseURI>/apps/<clientName>.mdon"`
    *
    * @return              A future response containing the Mastodon handle or an error.
    */
  def createApp(baseURI     : String,
                clientName  : String,
                scopes      : Set[Scope]  = Scope.all,
                redirectURIs: Seq[String] = Seq(DEFAULT_REDIRECT_URI),
                storageLoc  : String      = DEFAULT_STORAGE_LOC
               )
               (implicit system: ActorSystem,
                materializer: Materializer,
                ec: ExecutionContext): Future[Mastodon] = {
    val dir = new File(s"$storageLoc/$baseURI/apps")
    //check directory here, everything else that relies on it occurs beyond.
    if (!dir.exists()) {
      if (!dir.mkdirs()) {
        Future.failed(new Exception(s"Could not create or find API storage directory for this network: $dir"))
      }
    }

    val dataOpt = loadAppData(baseURI = baseURI, clientName = clientName, storageLoc = storageLoc)
    dataOpt match {
      case Some(cred) =>
        Future.successful(new Mastodon(baseURI, cred, scopes))
      case None =>
        val entity = Json.obj(
          "client_name"   -> clientName,
          "scopes"        -> scopes       .mkString(" "),
          "redirect_uris" -> redirectURIs .mkString(" ")
        ).toJsonEntity
        val req = HttpRequest(POST, uri = s"https://$baseURI/api/v1/apps", entity = entity)

        Http().singleRequest(req).flatMap(xhr =>
          xhr.as(AppCredentials).map { cred =>
            saveAppData(baseURI = baseURI, clientName = clientName, credentials = cred, storageLoc = storageLoc)
            new Mastodon(baseURI, cred, scopes)
          }
        )
    }
  }
}
