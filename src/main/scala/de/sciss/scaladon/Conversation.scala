package de.sciss.scaladon

import de.sciss.scaladon.Util.indent
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class Conversation(id                : Id,
                        accounts          : Seq[Account],
                        unread            : Boolean,
                        lastStatus        : Option[Status]
) {
  override def toString: String =
    s"""$productPrefix(
      |  id         = $id,
      |  accounts   = ${accounts.map(s => indent(s.toString))},
      |  unread     = $unread,
      |  lastStatus = ${lastStatus.map(s => indent(s.toString))},
      |)
      |""".stripMargin
}

object Conversation extends Reader[Conversation] {
  override final val name = "Conversation"

  implicit val reads: Reads[Conversation] = (
    (__ \ "id"          ).read[Id] and
    (__ \ "accounts"    ).read[Seq[Account]] and
    (__ \ "unread"      ).readWithDefault[Boolean](false) and
    (__ \ "last_status" ).readNullable[Status]
  )(Conversation.apply _)
}