package de.sciss.scaladon

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class Tag(name : String,
               url  : String
              )

object Tag extends Reader[Tag] {
  override final val name = "Tag"

  implicit val reads: Reads[Tag] = (
    (__ \ "name").read[String] and
    (__ \ "url" ).read[String]
  )(Tag.apply _)
}
