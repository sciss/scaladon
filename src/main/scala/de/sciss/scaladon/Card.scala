package de.sciss.scaladon

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class Card(title       : String,
                description : String,
                url         : String,
                image       : String
               )

object Card extends Reader[Card] {
  override final val name = "Card"

  implicit val reads: Reads[Card] = (
    (__ \ "title"       ).read[String] and
    (__ \ "description" ).read[String] and
    (__ \ "url"         ).read[String] and
    (__ \ "image"       ).read[String]
  )(Card.apply _)
}
