package de.sciss.scaladon

object Util {
  def indent(s: String, n: Int = 4, preNl: Boolean = true): String = {
    val in = " " * n
    s.split('\n').map(ln => s"$in$ln").mkString(if (preNl) "\n" else "", "\n", "")
  }
}
