package de.sciss.scaladon

import akka.http.scaladsl.model.StatusCode
import play.api.libs.json.{JsPath, JsonValidationError}

import scala.collection.{Seq => CSeq}

object MastodonError {
  case class JSONValidation(jsErrors: CSeq[(JsPath, CSeq[JsonValidationError])], detail: String)
    extends MastodonError(s"$detail\n$jsErrors")

  case class Response(statusCode: StatusCode, detail: String) extends MastodonError(s"$statusCode\n$detail")
}
abstract class MastodonError(message: String) extends RuntimeException(message)
