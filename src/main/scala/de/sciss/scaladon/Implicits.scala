package de.sciss.scaladon

import akka.http.scaladsl.model.{DateTime => _, _}
import akka.stream.Materializer
import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}

object Implicits {
  implicit class HttpResponseExtensions(resp: HttpResponse) {
    def as[A](reader: Reader[A])(implicit m: Materializer, ec: ExecutionContext): Future[A] =
      handleAs[A](reader.reads, reader.name)

    def asSeq[A](reader: Reader[A])(implicit m: Materializer, ec: ExecutionContext): Future[Seq[A]] =
      handleAs[Seq[A]](Reads.seq[A](reader.reads), s"Seq[${reader.name}]")

    private def handleAs[A](reads: Reads[A], name: String)(implicit m: Materializer, ec: ExecutionContext): Future[A] =
      resp.status match {
        case s if s.isSuccess =>
          resp.entity.toJsValue.map { json =>
            json.validate[A](reads) match {
              case JsSuccess(elem, _) => elem
              case e: JsError =>
                throw MastodonError.JSONValidation(e.errors, s"Error validating response JSON for $name.")
            }
          }
        case s if s.isFailure =>
          resp.entity.toJsValue.map { json =>
            val e = json.validate[Error] match {
              case JsSuccess(err, _)  => MastodonError.Response(resp.status, s"${err.error} for $name")
              case _: JsError         => MastodonError.Response(resp.status, s"An unknown error has occurred for $name.")
            }
            throw e
          }

        case _ =>
          Future.failed(MastodonError.Response(resp.status, "Unexpected response."))
      }
  }

  implicit class JsValueExtensions(json: JsValue) {
    def toJsonEntity: RequestEntity =
      HttpEntity(ContentTypes.`application/json`, json.toString)
  }

  implicit class ResponseEntityExtensions(entity: ResponseEntity) {
    def toJsValue(implicit m: Materializer, ec: ExecutionContext): Future[JsValue] = {
      val futByteString = entity.dataBytes.runReduce(_ concat _)
      futByteString.map { bs =>
        Json.parse(bs.toArray)
      }
    }
  }
}
