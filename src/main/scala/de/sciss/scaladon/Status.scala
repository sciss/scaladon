package de.sciss.scaladon

import de.sciss.scaladon.Util.indent
import play.api.libs.functional.syntax._
import play.api.libs.json._

import java.time.ZonedDateTime

case class Status(id                : Id,
                  uri               : String,
                  url               : String,
                  account           : Account,
                  inReplyToId       : Option[Id],
                  inReplyToAccountId: Option[Id],
                  reblog            : Option[Status],
                  content           : String,
                  createdAt         : ZonedDateTime,
                  reblogsCount      : Int,
                  favouritesCount   : Int,
                  reblogged         : Boolean,
                  favourited        : Boolean,
                  sensitive         : Boolean,
                  spoilerText       : Option[String],
                  visibility        : Visibility,
                  mediaAttachments  : Seq[Attachment],
                  mentions          : Seq[Mention],
                  tags              : Seq[Tag],
                  application       : Option[Application]
                 ) {
  override def toString: String =
    s"""$productPrefix(
       |  id                 = $id,
       |  uri                = $uri,
       |  url                = $url,
       |  account            = ${indent(account.toString)},
       |  inReplyToId        = $inReplyToId,
       |  inReplyToAccountId = $inReplyToAccountId,
       |  reblog             = ${reblog.map(s => indent(s.toString))},
       |  content            = $content,
       |  createdAt          = $createdAt,
       |  reblogsCount       = $reblogsCount,
       |  favouritesCount    = $favouritesCount,
       |  reblogged          = $reblogged,
       |  favourited         = $favourited,
       |  sensitive          = $sensitive,
       |  spoilerText        = $spoilerText,
       |  visibility         = $visibility,
       |  mediaAttachments   = ${mediaAttachments.map(s => indent(s.toString))},
       |  mentions           = ${mentions.map(s => indent(s.toString))},
       |  tags               = $tags,
       |  application        = $application,
       |)
       |""".stripMargin
}

object Status extends Reader[Status] {
  override final val name = "Status"

  implicit val reads: Reads[Status] = (
    (__ \ "id"                    ).read[Id] and
    (__ \ "uri"                   ).read[String] and
    (__ \ "url"                   ).read[String] and
    (__ \ "account"               ).read[Account] and
    (__ \ "in_reply_to_id"        ).readNullable[Id] and
    (__ \ "in_reply_to_account_id").readNullable[Id] and
    (__ \ "reblog"                ).lazyReadNullable[Status](reads) and
    (__ \ "content"               ).read[String] and
    (__ \ "created_at"            ).read[ZonedDateTime](DateTime.reads) and
    (__ \ "reblogs_count"         ).read[Int] and
    (__ \ "favourites_count"      ).read[Int] and
    (__ \ "reblogged"             ).readWithDefault[Boolean](false) and
    (__ \ "favourited"            ).readWithDefault[Boolean](false) and
    (__ \ "sensitive"             ).readWithDefault[Boolean](false) and
    (__ \ "spoiler_text"          ).read[String].map { str =>
      if(str.isEmpty) None
      else Some(str)
    } and
    (__ \ "visibility"            ).read[Visibility] and
    (__ \ "media_attachments"     ).read[Seq[Attachment]] and
    (__ \ "mentions"              ).read[Seq[Mention]] and
    (__ \ "tags"                  ).read[Seq[Tag]] and
    (__ \ "application"           ).readNullable[Application]
  )(Status.apply _)
}

sealed trait Visibility { self =>
  override def toString: String =
    self match {
      case Visibility.Default   => ""
      case Visibility.Direct    => "direct"
      case Visibility.Private   => "private"
      case Visibility.Public    => "public"
      case Visibility.Unlisted  => "unlisted"
    }
}

object Visibility {
  implicit val reads: Reads[Visibility] = __.read[String].map {
    case ""         => Visibility.Default
    case "direct"   => Visibility.Direct
    case "private"  => Visibility.Private
    case "unlisted" => Visibility.Unlisted
    case "public"   => Visibility.Public
  }

  final case object Default   extends Visibility
  final case object Direct    extends Visibility
  final case object Private   extends Visibility
  final case object Public    extends Visibility
  final case object Unlisted  extends Visibility
}

