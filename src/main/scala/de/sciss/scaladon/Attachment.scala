package de.sciss.scaladon

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class Attachment(id        : Id,
                      tpe       : AttachmentType,
                      url       : String,
                      remoteUrl : Option[String],
                      previewUrl: String,
                      textUrl   : Option[String],
                     ) {

  override def toString: String =
    s"""$productPrefix(
       |  id         = $id,
       |  tpe        = $tpe,
       |  url        = $url,
       |  remoteUrl  = $remoteUrl,
       |  previewUrl = $previewUrl,
       |  textUrl    = $textUrl,
       |)""".stripMargin
}

object Attachment extends Reader[Attachment] {
  override final val name = "Attachment"

  implicit val reads: Reads[Attachment] = (
    (__ \ "id"          ).read[Id] and
    (__ \ "type"        ).read[AttachmentType] and
    (__ \ "url"         ).read[String] and
    (__ \ "remote_url"  ).readNullable[String] and
    (__ \ "preview_url" ).read[String] and
    (__ \ "text_url"    ).readNullable[String]
  )(Attachment.apply _)
}

sealed trait AttachmentType { self =>
  override def toString: String = self match {
    case AttachmentType.Image  => "image"
    case AttachmentType.Video  => "video"
    case AttachmentType.Gifv   => "gifv"
  }
}

object AttachmentType {
  implicit val reads: Reads[AttachmentType] = __.read[String].map {
    case "image"  => AttachmentType.Image
    case "video"  => AttachmentType.Video
    case "gifv"   => AttachmentType.Gifv
  }

  final case object Image extends AttachmentType
  final case object Video extends AttachmentType
  final case object Gifv  extends AttachmentType
}
