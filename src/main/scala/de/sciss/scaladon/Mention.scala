package de.sciss.scaladon

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class Mention(accountId: Id,
                   username : String,
                   acct     : String,
                   url      : String
                  ) {
  override def toString: String =
    s"""$productPrefix(
       |  accountId = $accountId,
       |  username  = $username,
       |  acct      = $acct,
       |  url       = $url,
       |)""".stripMargin
}

object Mention extends Reader[Mention] {
  override final val name = "Mention"

  implicit val reads: Reads[Mention] = (
    (__ \ "id"      ).read[Id] and
    (__ \ "username").read[String] and
    (__ \ "acct"    ).read[String] and
    (__ \ "url"     ).read[String]
  )(Mention.apply _)
}
