package de.sciss.scaladon

import play.api.libs.json.Reads

trait Reader[A] {
  def name: String

  def reads: Reads[A]
}
