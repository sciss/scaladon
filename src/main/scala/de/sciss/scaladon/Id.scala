package de.sciss.scaladon
import play.api.libs.json.{Format, JsError, JsPath, JsResult, JsString, JsSuccess, JsValue, JsonValidationError}

import scala.collection.Seq

case class Id(value: String) extends AnyVal {
  override def toString: String = value
}

object Id extends Format[Id] {
  implicit def format: Format[Id] = this

  override def writes(o: Id): JsValue = JsString(o.value)

  override def reads(json: JsValue): JsResult[Id] = json match {
    case JsString(s) => JsSuccess(Id(s))
    case _           => JsError(Seq(JsPath -> Seq(JsonValidationError("error.expected.jsstring"))))
  }
}
