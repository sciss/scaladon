package de.sciss.scaladon

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class AppCredentials(id: Id, clientId: String, clientSecret: String)

object AppCredentials extends Reader[AppCredentials] {

  override final val name = "AppCredentials"

  implicit val reads: Reads[AppCredentials] = (
    (__ \ "id"            ).read[Id] and
    (__ \ "client_id"     ).read[String] and
    (__ \ "client_secret" ).read[String]
  )(AppCredentials.apply _)

  implicit val writes: Writes[AppCredentials] = (
    (__ \ "id"            ).write[Id] and
    (__ \ "client_id"     ).write[String] and
    (__ \ "client_secret" ).write[String]
  )(unlift(AppCredentials.unapply))
}
