package de.sciss.scaladon

import play.api.libs.functional.syntax._
import play.api.libs.json._

import java.time.ZonedDateTime

case class Notification(id        : Id,
                        tpe       : NotificationType,
                        createdAt : ZonedDateTime,
                        account   : Account,
                        status    : Option[Status]
                       )

object Notification extends Reader[Notification] {

  override final val name = "Notification"

  implicit val reads: Reads[Notification] = (
    (__ \ "id"        ).read[Id] and
    (__ \ "type"      ).read[NotificationType] and
    (__ \ "created_at").read[ZonedDateTime](DateTime.reads) and
    (__ \ "account"   ).read[Account] and
    (__ \ "status"    ).readNullable[Status]
  )(Notification.apply _)
}

sealed trait NotificationType { self =>
  override def toString: String = self match {
    case NotificationType.Mention    => "mention"
    case NotificationType.Reblog     => "reblog"
    case NotificationType.Favourite  => "favourite"
    case NotificationType.Follow     => "follow"
  }
}

object NotificationType {
  implicit val reads: Reads[NotificationType] = __.read[String].map {
    case "mention"    => NotificationType.Mention
    case "reblog"     => NotificationType.Reblog
    case "favourite"  => NotificationType.Favourite
    case "follow"     => NotificationType.Follow
  }

  final case object Mention   extends NotificationType
  final case object Reblog    extends NotificationType
  final case object Favourite extends NotificationType
  final case object Follow    extends NotificationType
}
