package de.sciss.scaladon

import play.api.libs.json.Reads

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

object DateTime extends Reader[ZonedDateTime] {
  override final val name = "DateTime"

  final val pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSz"

  private val formatter = DateTimeFormatter.ofPattern(pattern)

  implicit val reads: Reads[ZonedDateTime] = Reads[ZonedDateTime] { js =>
    js.validate[String].map { str =>
      ZonedDateTime.parse(str, formatter)
    }
  }
}
