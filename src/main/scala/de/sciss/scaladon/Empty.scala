package de.sciss.scaladon
import play.api.libs.json.{JsError, JsObject, JsSuccess, Reads}

object Empty extends Reader[Unit] {
  override final val name = "Empty"

  override val reads: Reads[Unit] = Reads[Unit] { js =>
    js.validate[JsObject].flatMap {
      case s: JsObject if s == JsObject.empty => JsSuccess(())
      case _ => JsError("Not an empty object")
    }
  }
}
