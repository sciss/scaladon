package de.sciss.scaladon

import play.api.libs.json._

case class Error(error: String)

object Error extends Reader[Error] {
  override final val name = "Error"

  implicit val reads: Reads[Error] = (__ \ "error").read[String].map(Error.apply)
}
