lazy val baseName       = "scaladon"
lazy val projectVersion = "0.5.1"

ThisBuild / version       := projectVersion
ThisBuild / organization  := "de.sciss"
ThisBuild / versionScheme := Some("pvp")

lazy val root = project.in(file("."))
  .settings(
    name          := baseName,
    licenses      := Seq("LGPL v2.1+" -> url("http://www.gnu.org/licenses/lgpl-2.1.txt")),
    homepage      := Some(url(s"https://github.com/Sciss/$baseName")),
    scalaVersion  := "2.13.7",
    crossScalaVersions := Seq(/*"3.1.0",*/ "2.13.7", "2.12.15"),
    libraryDependencies ++= Seq(
      // http and streams/
      "com.typesafe.akka" %% "akka-http"      % deps.main.akkaHttp,
      "com.typesafe.akka" %% "akka-http-core" % deps.main.akkaHttp,
      "com.typesafe.akka" %% "akka-stream"    % deps.main.akkaStream,
      // json
      "com.typesafe.play" %% "play-json"      % deps.main.playJson,
      // config
      "com.typesafe"      % "config"          % deps.main.typesafeConfig,
      // testing
      "org.scalactic"     %% "scalactic"      % deps.test.scalaTest % Test,
      "org.scalatest"     %% "scalatest"      % deps.test.scalaTest % Test,
    ),
  )
  .settings(publishSettings)

lazy val deps = new {
  val main = new {
    val akkaHttp       = "10.2.7"
    val akkaStream     = "2.6.17"
    val playJson       = "2.9.2" // "2.10.0-RC2"
    val typesafeConfig = "1.4.1"
  }
  val test = new {
    val scalaTest      = "3.2.10"
  }
}

// ---- publishing ----

lazy val publishSettings = Seq(
  publishMavenStyle       := true,
  Test / publishArtifact  := false,
  pomIncludeRepository    := { _ => false },
  developers := List(
    Developer(
      id    = "Mellow_",
      name  = "Mitchell Schwitzer",
      email = "mitchell@schwitzer.ca",
      url   = url("https://blog.schwitzer.ca")
    ),
    Developer(
      id    = "sciss",
      name  = "Hanns Holger Rutz",
      email = "contact@sciss.de",
      url   = url("https://www.sciss.de")
    )
  ),
  scmInfo := {
    val h = "github.com"
    val a = s"Sciss/$baseName"
    Some(ScmInfo(url(s"https://$h/$a"), s"scm:git@$h:$a.git"))
  },
)
