# scaladon

[![Build Status](https://github.com/Sciss/scaladon/workflows/Scala%20CI/badge.svg?branch=main)](https://github.com/Sciss/scaladon/actions?query=workflow%3A%22Scala+CI%22)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.sciss/scaladon_2.13/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.sciss/scaladon_2.13)

## statement

A [Mastodon](https://joinmastodon.org/) social network [API](https://docs.joinmastodon.org/client/intro/) 
implementation in Scala using Akka HTTP and Akka Streams.

This is a continuation of the original project - https://github.com/schwitzerm/scaladon - by Mitchell Schwitzer,
which was developed until Scala 2.12 and placed in the public domain.
This fork, maintained by Hanns Holger Rutz, tries to bring some things up-to-date. It is released under the 
GNU Lesser General Public License (LGPL) v2.1+. There is a distinct published Maven artifact (see _linking_).

## building

SoundProcesses builds with sbt against Scala 2.12, 2.13.
The dependencies should be downloaded automatically from Maven Central repository.

## linking

The following module is available, and can be used as sbt dependency:

    "de.sciss" %% "scaladon" % v

The current version `v` is `"0.5.1"`.

## What does not work yet

- changing user settings (display name/e-mail/avatar/header)

## Examples

Tooting a status:

```scala
package de.sciss.scaladon

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import de.sciss.scaladon._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object MyMastodonApp extends App {
  implicit val system: ActorSystem = ActorSystem()

  val statusFuture: Future[Status] = for {
    app <- Mastodon.createApp("botsin.space", "mycoolapp")
    token <- app.login("my@email.com", "thisshouldreallybsupersecure")
  } yield {
    app.toot("I'm tooting from the Scaladon API!", Visibility.Public)(token)
  }
}
```

Fetching an account:

```scala
package de.sciss.scaladon

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import de.sciss.scaladon._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object MyMastodonApp extends App {
  implicit val system: ActorSystem = ActorSystem()
  
  val accountFuture: Future[Account] = for {
    app     <- Mastodon.createApp("botsin.space", "mycoolapp")
    token   <- app.login("my@email.com", "thisshouldreallybsupersecure")
    account <- app.Accounts.fetch(Id("1"))(token) 
  } yield account
}
```
